#!/usr/bin/python

# DWhere v1.0 (20181220-0)
#
# convert SQLite database from Virtual Volumes View to DWhere format
#
# Copyright 2018-2019 - Domenico Ferrari <domfe@tiscali.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import sqlite3

databasevvv = 'elenco-custodie-from-vvv.sqlite'
database = 'elenco-custodie-dwhere.sqlite'

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)

    return None

def create_table(conn, create_table_sql):
    try:
        c.execute(create_table_sql)
    except sqlite3.Error as e:
        print(e)

def initdb():
    global conn

    sql_create_categories_table = """ CREATE TABLE IF NOT EXISTS categories (
                                        idcat integer PRIMARY KEY,
                                        description text NOT NULL
                                    ); """

    sql_create_disks_table = """CREATE TABLE IF NOT EXISTS disks (
                                    iddisk integer PRIMARY KEY,
                                    idcat integer NOT NULL,
                                    description text NOT NULL,
                                    FOREIGN KEY (idcat) REFERENCES categories (idcat)
                                );"""

    sql_create_paths_table = """CREATE TABLE IF NOT EXISTS paths (
                                    idpath integer PRIMARY KEY,
                                    iddisk integer NOT NULL,
                                    name text NOT NULL,
                                    idparent integer,
                                    mtime integer NOT NULL,
                                    FOREIGN KEY (iddisk) REFERENCES categories (iddisk)
                                );"""
                                
    sql_create_files_table = """CREATE TABLE IF NOT EXISTS files (
                                    idfile integer PRIMARY KEY,
                                    idpath integer NOT NULL,
                                    name text NOT NULL,
                                    size integer NOT NULL,
                                    mtime integer NOT NULL,
                                    FOREIGN KEY (idpath) REFERENCES paths (idpath)
                                );"""
                                
    sql_disk_trigger = """CREATE TRIGGER IF NOT EXISTS disk_delete AFTER DELETE
                          ON disks
                          BEGIN
                            DELETE FROM paths WHERE iddisk=OLD.iddisk;
                          END;"""
                             
    sql_path_trigger = """CREATE TRIGGER IF NOT EXISTS path_delete AFTER DELETE
                          ON paths
                          BEGIN
                            DELETE FROM files WHERE idpath=OLD.idpath;
                          END;"""
                             
    c = conn.cursor()
    try:
        c.execute(sql_create_categories_table)
        c.execute(sql_create_disks_table)
        c.execute(sql_create_paths_table)
        c.execute(sql_create_files_table)
        c.execute(sql_disk_trigger)
        c.execute(sql_path_trigger)
        conn.commit()
    except sqlite3.Error as e:
        print(e)

        
def insertFile(idparent, fname, fsize, mtime):
  global conn
  
  cur=conn.cursor()
  cur.execute('INSERT INTO files(idpath, name, size, mtime) VALUES(?, ?, ?, ?)', (idparent, fname, fsize, mtime))

  
def insertFolder(iddisk, idparent, vvvid, fname, fsize, mtime):
  global connvvv
  global conn
  
  cur=conn.cursor()
  cur.execute('INSERT INTO paths(iddisk, name, idparent, mtime) VALUES(?, ?, ?, ?)', (iddisk, fname, idparent, mtime))
  idparent=cur.lastrowid
  curvvv=connvvv.cursor()
  curvvv.execute("select f.path_file_id, f.file_name, f.file_size, strftime('%s', f.file_datetime)-14400 from files f where path_id=?", (vvvid,))
  rows=curvvv.fetchall()
  for row in rows:
    if row[0] != None:
      insertFolder(iddisk, idparent, row[0], row[1], row[2], row[3])
    else:
      insertFile(idparent, row[1], row[2], row[3])  
  

def insertDisk(idcat, vvvid, descr):
  global connvvv
  global conn
  
  cur=conn.cursor()
  cur.execute('INSERT INTO disks(idcat, description) VALUES(?, ?)', (idcat, descr))
  iddisk=cur.lastrowid
  cur.execute('INSERT INTO paths(iddisk, name, idparent, mtime) VALUES(?, ?, ?, ?)', (iddisk, '/', 0, 0))
  idparent=cur.lastrowid
  curvvv=connvvv.cursor()
  curvvv.execute("select f.path_file_id, f.file_name, f.file_size, strftime('%s', f.file_datetime)-14400 from virtual_files vf, files f where virtual_path_id=? and vf.physical_file_id=f.file_id", (vvvid,))
  rows=curvvv.fetchall()
  for row in rows:
    if row[0] != None:
      insertFolder(iddisk, idparent, row[0], row[1], row[2], row[3])
    else:
      insertFile(idparent, row[1], row[2], row[3])  

def insertCategory(vvvid, descr):
  global connvvv
  global conn
  
  print(descr)
  cur=conn.cursor()
  cur.execute('INSERT INTO categories(description) VALUES(?)', (descr,))
  idcat=cur.lastrowid
  conn.commit()
  curvvv=connvvv.cursor()
  curvvv.execute('select path_id, path from virtual_paths where father_id=? and phys_path_id is null', (vvvid,))
  rows=curvvv.fetchall()
  for row in rows:
    insertDisk(idcat, row[0], row[1])
  conn.commit()

def alterDB():
  global database

  conn = create_connection(database)
  with conn:
    cur=conn.cursor()
    cur.execute('DROP TRIGGER IF EXISTS disk_delete')
    cur.execute('DROP TRIGGER IF EXISTS path_delete')
    cur.execute('DROP TABLE IF EXISTS categories')
    cur.execute('DROP TABLE IF EXISTS disks')
    cur.execute('DROP TABLE IF EXISTS paths')
    cur.execute('DROP TABLE IF EXISTS files')
    conn.commit()

    
def main():
  global database
  global databasevvv
  global conn
  global connvvv

  conn = create_connection(database)
  connvvv = create_connection(databasevvv)

  initdb()

  curvvv=connvvv.cursor()
  curvvv.execute('select path_id, path from virtual_paths where father_id is null and phys_path_id is null')
  rows = curvvv.fetchall()
  for row in rows:
    insertCategory(row[0], row[1])
    
    
if __name__ == '__main__':
    alterDB()
    main()
