#!/usr/bin/python

# DWhere v1.0 (20181220-0)
#
# import data from firebird database of Virtual Volumes View
# the data are imported as-is in SQLite database
#
# Copyright 2018-2019 - Domenico Ferrari <domfe@tiscali.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import sqlite3
import fdb

fb_database='vvv.fb'
database='dwhere-vvv.sqlite'

def create_connection(db_file):
  """ create a database connection to the SQLite database
      specified by the db_file
  :param db_file: database file
  :return: Connection object or None
  """
  try:
    conn = sqlite3.connect(db_file)
    return conn
  except sqlite3.Error as e:
    print(e)

  return None


def create_table(conn, create_table_sql):
  try:
    c.execute(create_table_sql)
  except sqlite3.Error as e:
    print(e)


def initdb():
  global conn

  sql1="""CREATE TABLE IF NOT EXISTS virtual_paths (
        path_id integer PRIMARY KEY,
        path text NOT NULL,
        father_id integer CHECK((FATHER_ID IS NULL) OR ( FATHER_ID <> PATH_ID)),
        phys_path_id integer,
        FOREIGN KEY (phys_path_id) REFERENCES paths (path_id),
        FOREIGN KEY (father_id) REFERENCES virtual_paths (path_id)
  );"""


  sql2="""CREATE TABLE IF NOT EXISTS virtual_files (
        FILE_ID integer PRIMARY KEY,
        VIRTUAL_PATH_ID  integer NOT NULL,
        PHYSICAL_FILE_ID  integer NOT NULL,
        VIRTUAL_PATH_FILE_ID  integer,
        FOREIGN KEY (PHYSICAL_FILE_ID) REFERENCES FILES (FILE_ID),
        FOREIGN KEY (VIRTUAL_PATH_ID) REFERENCES VIRTUAL_PATHS (PATH_ID),
        FOREIGN KEY (VIRTUAL_PATH_FILE_ID) REFERENCES VIRTUAL_PATHS (PATH_ID)
  );"""


  sql3="""CREATE TABLE IF NOT EXISTS volumes (
        VOLUME_ID integer PRIMARY KEY,
        VOLUME_NAME text NOT NULL,
        VOLUME_DESCRIPTION text
  );"""

  sql4="""CREATE UNIQUE INDEX IF NOT EXISTS UNQ_VOLUMES ON volumes (VOLUME_NAME);"""


  sql5="""CREATE TABLE IF NOT EXISTS files(
        FILE_ID integer PRIMARY KEY,
        FILE_NAME text,
        FILE_SIZE integer,
        FILE_EXT text,
        FILE_DATETIME integer,
        PATH_ID integer,
        PATH_FILE_ID integer,
        FILE_DESCRIPTION text,
        FOREIGN KEY (PATH_ID) REFERENCES PATHS (PATH_ID),
        FOREIGN KEY (PATH_FILE_ID) REFERENCES PATHS (PATH_ID)
  );
  """

  sql6="""CREATE TABLE IF NOT EXISTS paths(
        PATH_ID integer PRIMARY KEY,
        VOLUME_ID integer NOT NULL,
        PATH_NAME text NOT NULL,
        FATHER_ID integer CHECK((FATHER_ID <> PATH_ID) OR (FATHER_ID IS NULL)),
        PATH_DESCRIPTION text,
        FOREIGN KEY (FATHER_ID) REFERENCES PATHS (PATH_ID),
        FOREIGN KEY (VOLUME_ID) REFERENCES VOLUMES (VOLUME_ID)
  );"""



  c = conn.cursor()
  try:
    c.execute(sql3)
    c.execute(sql4)
    c.execute(sql6)
    c.execute(sql5)
    c.execute(sql1)
    c.execute(sql2)
    conn.commit()
  except sqlite3.Error as e:
    print('SQL ERROR!')
    print(e)


def loadVVV():
  global conn

  file=open('vvv-data.csv', 'r')
  for line in file:
    csv=line.split(',')

    print(csv[0][1:-1])
  file.close()


def importVVV():
  global conn
  global fconn

  cur=conn.cursor()
  fcur=fconn.cursor()
  fcur.execute(u'SELECT VOLUME_ID, VOLUME_NAME, VOLUME_DESCRIPTION FROM volumes')
  row=fcur.fetchone()
  while row != None:
    cur.execute(u'INSERT INTO volumes(VOLUME_ID, VOLUME_NAME, VOLUME_DESCRIPTION) VALUES(?,?,?)', row)
    row=fcur.fetchone()
  conn.commit()  

  cur=conn.cursor()
  fcur=fconn.cursor()
  fcur.execute(u'SELECT PATH_ID, VOLUME_ID, PATH_NAME, FATHER_ID, PATH_DESCRIPTION FROM paths')
  row=fcur.fetchone()
  while row != None:
    cur.execute(u'INSERT INTO paths(PATH_ID, VOLUME_ID, PATH_NAME, FATHER_ID, PATH_DESCRIPTION) VALUES(?,?,?,?,?)', row)
    row=fcur.fetchone()
  conn.commit()  

  cur=conn.cursor()
  fcur=fconn.cursor()
  fcur.execute(u'SELECT FILE_ID, FILE_NAME, FILE_SIZE, FILE_EXT, FILE_DATETIME, PATH_ID, PATH_FILE_ID, FILE_DESCRIPTION FROM files')
  row=fcur.fetchone()
  while row != None:
    cur.execute(u'INSERT INTO files(FILE_ID, FILE_NAME, FILE_SIZE, FILE_EXT, FILE_DATETIME, PATH_ID, PATH_FILE_ID, FILE_DESCRIPTION) VALUES(?,?,?,?,?,?,?,?)', row)
    row=fcur.fetchone()
  conn.commit()  

  cur=conn.cursor()
  fcur=fconn.cursor()
  fcur.execute(u'SELECT PATH_ID, PATH, FATHER_ID, PHYS_PATH_ID FROM virtual_paths')
  row=fcur.fetchone()
  while row != None:
    cur.execute(u'INSERT INTO virtual_paths(PATH_ID, PATH, FATHER_ID, PHYS_PATH_ID) VALUES(?,?,?,?)', row)
    row=fcur.fetchone()
  conn.commit()  

  cur=conn.cursor()
  fcur=fconn.cursor()
  fcur.execute(u'SELECT FILE_ID, VIRTUAL_PATH_ID, PHYSICAL_FILE_ID, VIRTUAL_PATH_FILE_ID FROM virtual_files')
  row=fcur.fetchone()
  while row != None:
    cur.execute(u'INSERT INTO virtual_files(FILE_ID, VIRTUAL_PATH_ID, PHYSICAL_FILE_ID, VIRTUAL_PATH_FILE_ID) VALUES(?,?,?,?)', row)
    row=fcur.fetchone()
  conn.commit()  


def deletedb():
  global conn
  
  cur=conn.cursor()
  cur.execute('DROP TABLE virtual_files')
  cur.execute('DROP TABLE virtual_paths')
  cur.execute('DROP TABLE files')
  cur.execute('DROP TABLE paths')
  cur.execute('DROP TABLE volumes')
  conn.commit()

  
def main():
  global conn
  global fconn
  global database

  fconn=fdb.connect(dsn='/home/pi/test.vvv', user='sysdba', password='sysdba', charset='utf-8')

  # create a database connection
  conn = create_connection(database)
  with conn:
    deletedb()
    initdb()
    importVVV()
    #loadVVV()


if __name__ == '__main__':
    main()

    
    
    
# categories
# select path_id, path from virtual_paths where father_id is null and phys_path_id is null

# disks
# select path_id, path from virtual_paths where father_id = <category_id> and phys_path_id is null
# select path_id, path from virtual_paths where father_id = 1 and phys_path_id is null

# root files and folders
# select * from virtual_files vf, files f where virtual_path_id=<disk_id> and vf.physical_file_id=f.file_id

# files and folders
# select f.file_id, f.file_name, f.file_size, datetime(f.file_datetime, 'localtime', 'unixepoch'), f.path_file_id from files f where path_id=<path_file_id>

# select f.file_name, p.path_id, p.path_name, p.father_id from virtual_files vf, files f, paths p where virtual_path_id=11255 and vf.physical_file_id=f.file_id and f.path_id=p.path_id


