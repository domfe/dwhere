## Database structure

```
                                  CATEGORIES
    /-\----------------------(0,n)--- idcat
    R 1                                  
    \-/                                  
    |                                    
    |       DISKS                                       FILES
    |(1,1)- idcat                    /-\
                             (1,n)   R 3  (1,1)
            diskid ------------------\-/--------------- diskid
```

### Tables

    TABLE categories
      idcat integer                           --- category identifier
      description text NOT NULL               --- category description

    PRIMARY KEY=idcat


In this table the program stores the information about the categories.
  
    


    TABLE disks
      iddisk integer auto_increment           --- disk identifier
      idcat integer                           --- category identifier
      description text NOT NULL               --- disk description
  
    PRIMARY KEY=iddisk
    UNIQUE KEY=(idcat,iddisk)

Here the disks are stored and matched against categories.

```
TABLE files
  idfile integer auto_increment                    --- file identifier
  iddisk integer                                   --- disk identifier
  path text                                        --- file path
  name text                                        --- file name
  size integer                                     --- file size in bytes
  creation_date datetime                           --- creation time
  access_date datetime                             --- access time
  modification_date datetime                       --- modification time

PRIMARY KEY=idfile
UNIQUE KEY=(iddisk,idfile)
```

Files and directories are saved in this table.




## User interface

Action provided
1. browse files and folders of disks
2. catalog disk in category
3. search files and folders from a pattern
   and tell parent folder, disk and category
4. change disk order in category
5. change disk category

**Main Page**
```
c. Categories       -> to Category List Page
s. Search           -> to Search Page
0. Exit
```

**Category List Page**
```
n. New Category     -> to New Category Page

01. category 1
...
nn. category nn

b. Back
```

**Category Page**
```
<Category id> - <Category description>

m. Modify
d. Delete
n. New Disk

01. disk 1
...
nn. disk nn

b. Back
```

**Modify Category Page**
```
<Category id> - <Category description>
Leave empty to not change description
New description: _______________
```

**Delete Category Page**
```
<Category id> - <Category description>
Are you sure to delete category? (y/N)
```

**New Category Page**
```
Leave empty to not create category
Category description: ____________
```

**Search Page**
