#!/usr/bin/python

# DWhere v1.0 (20181220-0)
# Copyright 2018-2019 - Domenico Ferrari <domfe@tiscali.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import time
import sqlite3

database = 'dwhere.sqlite'

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)

    return None

def create_table(conn, create_table_sql):
    try:
        c.execute(create_table_sql)
    except sqlite3.Error as e:
        print(e)

def initdb(conn):
    sql_create_categories_table = """ CREATE TABLE IF NOT EXISTS categories (
                                        idcat integer PRIMARY KEY,
                                        description text NOT NULL
                                    ); """

    sql_create_disks_table = """CREATE TABLE IF NOT EXISTS disks (
                                    iddisk integer PRIMARY KEY,
                                    idcat integer NOT NULL,
                                    description text NOT NULL,
                                    FOREIGN KEY (idcat) REFERENCES categories (idcat)
                                );"""

    sql_create_paths_table = """CREATE TABLE IF NOT EXISTS paths (
                                    idpath integer PRIMARY KEY,
                                    iddisk integer NOT NULL,
                                    name text NOT NULL,
                                    idparent integer,
                                    mtime integer NOT NULL,
                                    FOREIGN KEY (iddisk) REFERENCES categories (iddisk)
                                );"""
                                
    sql_create_files_table = """CREATE TABLE IF NOT EXISTS files (
                                    idfile integer PRIMARY KEY,
                                    idpath integer NOT NULL,
                                    name text NOT NULL,
                                    size integer NOT NULL,
                                    mtime integer NOT NULL,
                                    FOREIGN KEY (idpath) REFERENCES paths (idpath)
                                );"""
                                
    sql_disk_trigger = """CREATE TRIGGER IF NOT EXISTS disk_delete AFTER DELETE
                          ON disks
                          BEGIN
                            DELETE FROM paths WHERE iddisk=OLD.iddisk;
                          END;"""
                             
    sql_path_trigger = """CREATE TRIGGER IF NOT EXISTS path_delete AFTER DELETE
                          ON paths
                          BEGIN
                            DELETE FROM files WHERE idpath=OLD.idpath;
                          END;"""
                             
    c = conn.cursor()
    try:
        c.execute(sql_create_categories_table)
        c.execute(sql_create_disks_table)
        c.execute(sql_create_paths_table)
        c.execute(sql_create_files_table)
        c.execute(sql_disk_trigger)
        c.execute(sql_path_trigger)
        conn.commit()
    except sqlite3.Error as e:
        print(e)


def pageHeader(pagetxt):
  print('')
  print('------------------------------------------------------------')
  print('DWhere - %s' % (pagetxt,))
  print('------------------------------------------------------------')


def getSelection(msg=''):
  q=''
  try:
    if sys.version_info[0] < 3:
      q=raw_input(msg).decode(sys.stdin.encoding)
    else:
      q=input(msg)
  except:
    q=''
  return q.lower()

  
def getDisk(iddisk):
  global conn
  
  cur = conn.cursor()
  cur.execute('SELECT c.description, d.description FROM disks d, categories c WHERE c.idcat=d.idcat AND iddisk=?', (iddisk,));
  row = cur.fetchone()
  return {'category': row[0], 'description': row[1]}

  
def getPath(idpath):
  global conn
  
  cur = conn.cursor()
  cur.execute('SELECT iddisk, name, idparent FROM paths WHERE idpath=?', (idpath,));
  row = cur.fetchone()
  path=''
  while row[2] != 0:
    path = '/' + row[1] + path
    cur.execute('SELECT iddisk, name, idparent FROM paths WHERE idpath=?', (row[2],));
    row = cur.fetchone()
    
  return {'iddisk': row[0], 'name': path}

  
def newDisk(iddisk, path):
  global conn

  cur=conn.cursor()
  path=unicode(path)
  path=path.replace('\\', '/')
  if path[-1]=='/':
    path=path[:-1]
  content=os.walk(path);
  insertpath='INSERT INTO paths(iddisk, name, idparent, mtime) VALUES(?, ?, ?, ?)'
  insertfile='INSERT INTO files(idpath, name, size, mtime) VALUES(?, ?, ?, ?)'
  idpath={}
  st=os.stat(path)
  cur.execute(insertpath, (iddisk, '/', 0, st.st_mtime))
  idpath[path]=cur.lastrowid
  for root, dirs, files in content:
    root=root.replace('\\', '/')

    for dir in dirs:
      path=root + '/' + dir
      st=os.stat(path)
      cur.execute(insertpath, (iddisk, dir, idpath[root], st.st_mtime))
      idpath[path]=cur.lastrowid
      
    for file in files:
      st=os.stat(root + '/' + file)
      cur.execute(insertfile, (idpath[root], file, st.st_size, st.st_mtime))
  conn.commit()

  
def pageDiskModify(iddisk):
  global conn

  pageHeader('Modify Disk')
  cur = conn.cursor()
  cur.execute('SELECT iddisk, description FROM disks WHERE iddisk = ?', (iddisk,))
  row = cur.fetchone()
  print('%d - %s' % (row[0], row[1]))
  print('Leave empty to not change description')
  sel=getSelection('New description:')
  if sel != '':
    cur = conn.cursor()
    try:
      cur.execute('UPDATE disks SET description = ? WHERE iddisk = ?', (sel, iddisk))
      conn.commit()
      print('Disk modified!')
    except sqlite3.Error as e:
      print('Error modifying disk!')
    sel=getSelection()

def pageDiskDelete(iddisk):
  global conn

  pageHeader('Delete Disk')
  cur = conn.cursor()
  cur.execute('SELECT iddisk, description FROM disks WHERE iddisk = ?', (iddisk,))
  row = cur.fetchone()
  print('%d - %s' % (row[0], row[1]))
  print('Are you sure to delete disk? (y/N)')
  sel=getSelection()
  if sel == 'y':
    cur = conn.cursor()
    try:
      cur.execute('DELETE FROM disks WHERE iddisk = ?', (iddisk,))
      conn.commit()
      print('Disk deleted!')
    except sqlite3.Error as e:
      print('Error deleting disk!')
    sel=getSelection()

def pageFile(iddisk,idfile,path):
  global conn

  cur = conn.cursor()

  sel=''
  while sel != 'b':
    cur.execute("SELECT description FROM disks WHERE iddisk = ?", (iddisk,))
    diskrow = cur.fetchone()
    if diskrow == None:
      return

    pageHeader('Disk file - ' + diskrow[0])

    cur = conn.cursor()
    cur.execute("SELECT name, datetime(mtime, 'unixepoch', 'localtime'), size FROM files WHERE idfile=?", (idfile,))
    row = cur.fetchone()

    print('')
    print(' File name: ' + row[0])
    if path == '':
      print(' Directory: ' + '/')
    else:
      print(' Directory: ' + path)
    print(' Modification time: ' + row[1])
    
    size=row[2]
    ssize='B'
    if size > 1024:
      size=size / 1024
      ssize='KB'
      
    if size > 1024:
      size=size / 1024
      ssize='MB'
      
    print(' Size: ' + str(size) + ' ' + ssize)
    print('')

    print(' b. Back')
    print('Select an action: ')
    sel=getSelection()
        
        
def pageFiles(iddisk,idpath,parentpath):
  global conn

  cur = conn.cursor()

  sel=''
  while sel != 'b':
    cur.execute("SELECT description FROM disks WHERE iddisk = ?", (iddisk,))
    diskrow = cur.fetchone()
    if diskrow == None:
      return

    pageHeader('Disk files - ' + diskrow[0])

    cur = conn.cursor()
    cur.execute("SELECT name, datetime(mtime, 'unixepoch', 'localtime') FROM paths WHERE idpath=?", (idpath,))
    row = cur.fetchone()
    path=parentpath + '/' + row[0]
    
    print(path + ' (' + row[1] + ')')
    print('')

    cur = conn.cursor()
    cur.execute('SELECT idpath, name FROM paths WHERE idparent=?', (idpath,))
    rows = cur.fetchall()
    obj=list()
    if len(rows)>0:
      for row in rows:
        obj.append({'type': 0, 'id': row[0]})
        print(' %6d. %s/' % (len(obj), row[1]))

    cur.execute('SELECT idfile, name FROM files WHERE idpath=?', (idpath,))
    rows = cur.fetchall()
    if len(rows)>0:
      for row in rows:
        obj.append({'type': 1, 'id': row[0]})
        print(' %6d. %s' % (len(obj), row[1]))

    if len(obj)==0:
      print('No files')

    print('')
    print(' b. Back')
    print(' x. Exit')
    print('Select an action: ')
    sel=getSelection()
    if sel == 'x':
      raise SystemExit(0)
    elif sel.isdigit() and int(sel) > 0 and int(sel) <= len(obj):
      if obj[int(sel)-1]['type'] == 1:
        pageFile(iddisk, obj[int(sel)-1]['id'], path)
      else:
        pageFiles(iddisk, obj[int(sel)-1]['id'], path)
        

def pageDisk(iddisk):
  global conn

  cur = conn.cursor()

  sel=''
  while sel != 'b':
    cur.execute('SELECT iddisk, description FROM disks WHERE iddisk = ?', (iddisk,))
    row = cur.fetchone()
    if row == None:
      return

    pageHeader('Disk - ' + row[1])

    print('')
    print(' m. Modify')
    print(' d. Delete')
    print('')

    cur = conn.cursor()
    # get root folder of disk
    cur.execute('SELECT idpath FROM paths WHERE iddisk=? AND idparent=?', (iddisk,0))
    row = cur.fetchone()
    idparent=row[0]
    
    cur.execute('SELECT idpath, name FROM paths WHERE iddisk=? AND idparent=?', (iddisk,idparent))
    rows = cur.fetchall()
    obj=list()
    if len(rows)>0:
      for row in rows:
        obj.append({'type': 0, 'id': row[0]})
        print(' %6d. %s/' % (len(obj), row[1]))

    cur.execute('SELECT idfile, name FROM files WHERE idpath=?', (idparent,))
    rows = cur.fetchall()
    if len(rows)>0:
      for row in rows:
        obj.append({'type': 1, 'id': row[0]})
        print(' %6d. %s' % (len(obj), row[1]))

    if len(obj)==0:
      print('No files')

    print('')
    print(' b. Back')
    print(' x. Exit')
    print('Select an action: ')
    sel=getSelection()
    if sel == 'x':
      raise SystemExit(0)
    elif sel == 'm':
      pageDiskModify(iddisk)
    elif sel == 'd':
      pageDiskDelete(iddisk)
    elif sel.isdigit() and int(sel) > 0 and int(sel) <= len(obj):
      if obj[int(sel)-1]['type'] == 1:
        pageFile(iddisk, obj[int(sel)-1]['id'], '')
      else:
        pageFiles(iddisk, obj[int(sel)-1]['id'], '')
  
  
def pageDiskNew(idcat):
  global conn

  print('Leave empty to not create disk')
  path=getSelection('Disk path: ')
  if path != '':
    cur=conn.cursor()
    cur.execute('INSERT INTO disks(idcat,description) VALUES(?,?)', (idcat,path))
    conn.commit()
    iddisk=cur.lastrowid;
    newDisk(iddisk, path)


def pageCategoryNew():
  global conn

  print('Leave empty to not create category')
  descr=getSelection('Category description: ')
  if descr != '':
    cur=conn.cursor()
    cur.execute('INSERT INTO categories(description) VALUES(?)', (descr,))
    conn.commit()

def pageCategoryModify(idcat):
  global conn

  pageHeader('Modify Category')
  cur = conn.cursor()
  cur.execute('SELECT idcat, description FROM categories WHERE idcat = ?', (idcat,))
  row = cur.fetchone()
  print('%d - %s' % (row[0], row[1]))
  print('Leave empty to not change description')
  sel=getSelection('New description:')
  if sel != '':
    cur = conn.cursor()
    try:
      cur.execute('UPDATE categories SET description = ? WHERE idcat = ?', (sel, idcat))
      conn.commit()
      print('Category modified!')
    except sqlite3.Error as e:
      print('Error modifying category!')
    sel=getSelection()

def pageCategoryDelete(idcat):
  global conn

  pageHeader('Delete Category')
  cur = conn.cursor()
  cur.execute('SELECT idcat, description FROM categories WHERE idcat = ?', (idcat,))
  row = cur.fetchone()
  print('%d - %s' % (row[0], row[1]))

  cur = conn.cursor()
  cur.execute('SELECT iddisk FROM disks WHERE idcat = ?', (idcat,))
  row = cur.fetchone()
  if row == None:
    print('Are you sure to delete category? (y/N)')
    sel=getSelection()
    if sel == 'y':
      cur = conn.cursor()
      try:
        cur.execute('DELETE FROM categories WHERE idcat = ?', (idcat,))
        conn.commit()
        print('Category deleted!')
      except sqlite3.Error as e:
        print('Error deleting category!')
      sel=getSelection()
  else:
    print('Category not empty, cannot delete!')
    sel=getSelection()

def pageCategory(idcat):
  global conn

  cur = conn.cursor()

  sel=''
  while sel != 'b':
    pageHeader('Category')

    cur.execute('SELECT idcat, description FROM categories WHERE idcat = ?', (idcat,))
    row = cur.fetchone()
    if row == None:
      return

    print(' %d - %s' % (row[0], row[1]))
    print('')
    print(' m. Modify')
    print(' d. Delete')
    print(' n. New Disk')
    print('')

    cur = conn.cursor()
    cur.execute('SELECT iddisk, idcat, description FROM disks WHERE idcat = ? ORDER BY description COLLATE NOCASE ASC', (idcat,))
    rows = cur.fetchall()
    disk=list()
    if len(rows)>0:
      for row in rows:
        disk.append(row[0])
        print(' %02d. %s' % (len(disk), row[2]))
    else:
      print('No disks')

    print('')
    print(' b. Back')
    print('Select an action: ')
    sel=getSelection()
    if sel == 'm':
      pageCategoryModify(idcat)
    elif sel == 'd':
      pageCategoryDelete(idcat)
    elif sel == 'n':
      pageDiskNew(idcat)
    elif sel.isdigit() and int(sel) > 0 and int(sel) <= len(disk):
      pageDisk(disk[int(sel)-1])


def pageCategories():
  global conn

  sel=''
  while sel != 'b':
    pageHeader('Category List')
    print(' n. New Category')
    print('')

    cur = conn.cursor()
    cur.execute('SELECT idcat, description FROM categories ORDER BY description COLLATE NOCASE ASC')

    rows = cur.fetchall()

    cat=list()
    for row in rows:
      cat.append(row[0])
      print(' %02d. %s' % (len(cat), row[1]))

    print('')
    print(' b. Back')
    print('Select an action: ')
    sel=getSelection()

    if sel == 'n':
      pageCategoryNew()
    elif sel.isdigit() and int(sel) > 0 and int(sel) <= len(cat):
      pageCategory(cat[int(sel)-1])

  return


def pageSearch():
  global conn
  
  sel='A'
  while sel != '':
    pageHeader('Search file or folder')
    print(' Enter search pattern (leave empty to abort):')
    print('')
    sel=getSelection();
    if sel != '' and sel != '/':
      s='%' + sel + '%'
      cur = conn.cursor()
      # search folders
      cur.execute('SELECT idpath, iddisk, name, idparent FROM paths WHERE name LIKE ? ORDER BY iddisk, name', (s,));
      rows = cur.fetchall()
      obj = list()
      for row in rows:
        path=getPath(row[3])
        obj.append({'type': 0, 'id': row[0], 'iddisk': row[1], 'path': path['name']})
        disk=getDisk(row[1])
        print(' %6d. %s' % (len(obj), disk['category'] + ' ' + disk['description'] + ' ' + path['name'] + '/ ' + row[2] + '/'))
  
      # search files
      cur.execute('SELECT idfile, idpath, name FROM files WHERE name LIKE ? ORDER BY idpath, name', (s,));
      rows = cur.fetchall()
      for row in rows:
        path=getPath(row[1])
        iddisk=path['iddisk']
        obj.append({'type': 1, 'id': row[0], 'iddisk': iddisk, 'path': path['name']})
        disk=getDisk(iddisk)
        print(' %6d. %s' % (len(obj), disk['category'] + ' ' + disk['description'] + ' ' + path['name'] + '/ ' + row[2]))
         
      if len(obj)==0:
        print('No files')
      else:
        print('')
        print('Select an action: ')
        fsel=getSelection()
        if fsel.isdigit() and int(fsel) > 0 and int(fsel) <= len(obj):
          curobj=obj[int(fsel)-1]
          if curobj['type'] == 1:
            pageFile(curobj['iddisk'], curobj['id'], curobj['path'])
          else:
            pageFiles(curobj['iddisk'], curobj['id'], curobj['path'])


def pageQuery():
  global conn
  
  sel='A'
  while sel != '':
    pageHeader('Query DB')
    print(' Insert SQL statement (leave empty to abort):')
    print('')
    sel=getSelection();
    if sel != '':
      try:
        cur = conn.cursor()
        cur.execute(sel);
        conn.commit()
        rows = cur.fetchall()
        for row in rows:
          print(row)
      except sqlite3.Error as e:
        print(e)
        raw_input('Press a key to continue...')
  
  
def pageMain():
  sel=''
  while sel != 'x':
    pageHeader('Main')
    print(' c. Categories')
    print(' s. Search')
    print(' q. Query DB')
    print(' x. Exit')
    print('Select an action: ')
    sel=getSelection()

    if sel == 'c':
      pageCategories()
    elif sel == 's':
      pageSearch()
    elif sel == 'q':
      pageQuery()

  return

def main():
  global conn
  global database

  # create a database connection
  conn = create_connection(database)
  with conn:
    initdb(conn)
    pageMain()

def alterDB():
  global database

  conn = create_connection(database)
  with conn:
    cur=conn.cursor()
    cur.execute('DROP TABLE disks')
    cur.execute('DROP TABLE files')


def mainTest():
  global database

  conn = create_connection(database)
  cur=conn.cursor()

  path='c:\\dati\\apri-archivio\\archivio\\'
  path=unicode(path)
  path=path.replace('\\', '/')
  if path[-1]=='/':
    path=path[:-1]
  content=os.walk(path);
  print(os.stat(path))
  for root, dirs, files in content:
    root=root.replace('\\', '/')
    for file in files:
      cur.execute('SELECT * FROM files WHERE path = ?', (root,))
      st=os.stat(root + '/' + file)
      # print('%s %s %d %s %s %s' % (root[len(path):], file, st.st_size, time.ctime(st.st_ctime), time.ctime(st.st_mtime), time.ctime(st.st_atime)))
      print('%s %s' % (root[len(path):], file))

if __name__ == '__main__':
    #alterDB()
    main()
